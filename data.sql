-- USERS
CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL UNIQUE, position INT);

CREATE TABLE items (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, scores TEXT, score INT, description TEXT);

INSERT INTO users (name, position) values ('ben0', 0);
INSERT INTO users (name, position) values ('ben1', 1);

INSERT INTO items (name,scores,score,description) values('Bees knees', '{data:{}}', 0, 'Gin, Honey and Citrus');
INSERT INTO items (name,scores,score,description) values('Sex on the beach', '{data:{}}', 0, 'Hope you brought a blanket');
INSERT INTO items (name,scores,score,description) values('Paper Plane', '{data:{}}', 0, 'My goto drink');
INSERT INTO items (name,scores,score,description) values('Paloma', '{data:{}}', 0, 'A damn good band');
INSERT INTO items (name,scores,score,description) values('Old Fationed', '{data:{}}', 0, 'The clasic cocktail');
INSERT INTO items (name,scores,score,description) values('French 75', '{data:{}}', 0, 'Daijas goto drink');
INSERT INTO items (name,scores,score,description) values('Last Word', '{data:{}}', 0, 'Gin, Chartreuse and Death?');
