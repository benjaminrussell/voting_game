module [findItemById, allItems, updateItemScore]
import pf.SQLite3
import pf.Task exposing [Task]

Item : {
    id : I64,
    name : Str,
    score : I64,
}
ItemDesc : {
    id : I64,
    name : Str,
    score : I64,
    description : Str,
}

findItemById : I64 -> Task ItemDesc _
findItemById = \id ->
    SQLite3.execute {
        path: "./data.db",
        query: "SELECT id, name, score, description FROM items WHERE id = :id;",
        bindings: [{ name: ":id", value: Num.toStr id }],
    }
    |> Task.mapErr SqlError
    |> Task.await \rows ->
        when rows is
            [] -> Task.err ItemIdNotFound
            [[Integer _, String name, Integer score, String description], ..] -> Task.ok { id: id, name, score, description }
            _ -> Task.err (UnexpectedValues "got $(Inspect.toStr rows)")

parseListRows : List (List SQLite3.Value), List Item -> Result (List Item) _
parseListRows = \rows, acc ->
    when rows is
        [] -> acc |> Ok
        [[Integer id, String name, Integer score], .. as rest] ->
            parseListRows rest (List.append acc { id, name, score })

        _ -> Inspect.toStr rows |> UnexpectedSQLValues |> Err

allItems : Task (List Item) _
allItems =
    SQLite3.execute {
        path: "./data.db",
        query: "SELECT id, name, score FROM items;",
        bindings: [],
    }
    |> Task.mapErr SqlError
    |> Task.await \rows -> parseListRows rows [] |> Task.fromResult

updateItemScore : I64, I64 -> Task {} _
updateItemScore = \itemId, scoreToAdd ->
    SQLite3.execute {
        path: "./data.db",
        query: "UPDATE items SET score = score + :scoreToAdd WHERE id = :itemId",
        bindings: [{ name: ":scoreToAdd", value: Num.toStr scoreToAdd }, { name: ":itemId", value: Num.toStr itemId }],
    }
    |> Task.mapErr SqlError
    |> Task.map \_ -> {}
