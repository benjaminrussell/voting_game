module [setUser, findUserByName, updateUserPosition]
import pf.SQLite3
import pf.Task exposing [Task]

UserInfo : {
    name : Str,
    id : I64,
    position : I64,
}

setUser : Str -> Task {} _
setUser = \name ->
    SQLite3.execute {
        path: "./data.db",
        query: "INSERT INTO users (name, position) VALUES (:name, 0);",
        bindings: [{ name: ":name", value: name }],
    }
    |> Task.mapErr SqlError
    |> Task.map \_ -> {}

findUserByName : Str -> Task UserInfo _
findUserByName = \name ->
    SQLite3.execute {
        path: "./data.db",
        query: "SELECT id, name, position FROM users WHERE name = :name;",
        bindings: [{ name: ":name", value: name }],
    }
    |> Task.mapErr SqlError
    |> Task.await \rows ->
        when rows is
            [] -> Task.err UserNotFound
            [[Integer id, String _, Integer position], ..] -> Task.ok { id, name: name, position }
            _ -> Task.err (UnexpectedValues "got $(Inspect.toStr rows)")

updateUserPosition : I64, I64 -> Task {} _
updateUserPosition = \newPosition, userId ->
    SQLite3.execute {
        path: "./data.db",
        query: "UPDATE users SET position = position + :newPosition WHERE id = :userId",
        bindings: [{ name: ":newPosition", value: Num.toStr newPosition }, { name: ":userId", value: Num.toStr userId }],
    }
    |> Task.mapErr SqlError
    |> Task.map \_ -> {}
