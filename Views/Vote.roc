module [view]

view = \user, item ->
    """
        $(htmlContent user item)

        <script>
            $(jsContent)
        </script>

        <style>
            $(cssContent)
        </style>     
    """

html = \string -> string
css = \string -> string
# js = \string -> string

htmlContent = \user, item ->
    html
        """
            <section id="vote_view" class="page_view">
                <div id="vote_info">
                    <h1 id="title" class="title">Vote</h1>
                    <div>
                        <p>hi $(user.name) your id is $(Num.toStr user.id) vote position is $(Num.toStr user.position) 
                            <br>
                        item name $(item.name) item id is $(Num.toStr item.id) item score is $(Num.toStr item.score)
                        </p>
                    </div>
                </div>
                <form method="PUT" action="/api/updateScore" id="vote_form" class="card">
                    <div id="item_text">
                        <h1 id="item_name">$(item.name)</h1>
                        <span>$(item.description)</span>
                        <div class="devider"></div>
                    </div>
                    <div id="vote_inputs">
                    <label class="label">
                        Please rate the drink (1: hate it) - (10: love it)
                        <input type="number" value="0" min="1" max="10" name="score" class="input" required></input>
                    </label>
                    <button class="input">do stuff</button>
                    </div>
                </form>
            </section>
        """

cssContent =
    css
        """
            @scope(#vote_view){
                #item_name {
                    font-size: 2rem;
                    background: var(--gradient);
                    -webkit-background-clip: text;
                    -webkit-text-fill-color: transparent;
                    margin: 0;
                  }
                #vote_info {
                    grid-area: info;

                }
                #vote_form{
                    align-self: start;
                    grid-area: content;
                    grid-template-rows: auto 1fr;
                    gap: var(--gap);
                    #vote_inputs {
                        display: grid;
                        grid-template-rows: auto auto;
                        gap: var(--gap);
                        align-self: start;
                    }
                }
            }
        """

jsContent = ""
