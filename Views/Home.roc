module [view]

view =
    """
        $(htmlContent)

        <script>
            $(jsContent)
        </script>

        <style>
            $(cssContent)
        </style>     
    """

html = \string -> string
css = \string -> string
# js = \string -> string

htmlContent =
    html
        """
            <h1>HOME</h1>
            <a href="/view/login"><button>Login</button></a>

        """

cssContent =
    css
        """
        .text {
            background: red;
        }
        """

jsContent = ""
