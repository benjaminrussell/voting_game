module [view]
import pf.Task exposing [Task]

view = \items ->
    """
        $(htmlContent items)

        <script>
            $(jsContent)
        </script>

        <style>
            $(cssContent)
        </style>     
    """

html = \string -> string
css = \string -> string
# js = \string -> string

htmlContent = \items ->
    html
        """
        <section id="score_view" class="page_view">
            <div id="score_info">
                <h1 class="title">Scoreboard!</h1>
            </div>
            <div id="score_grid">
                $(parseItems items)
            </div>
        </section>

        """

cssContent =
    css
        """
        @scope(#score_view){
            #score_info {
                grid-area: info;
            }
            #score_grid {
                display: grid;
                gap: var(--gap);
                grid-template-rows: repeat(auto-fit, 60px);
                grid-area: content;

                .score_row {
                    font-size: clamp(1rem, 3vw, 1.5rem);
                    font-weight: bold;
                    display: grid;
                    grid-template-columns: 1fr 1fr;
                    gap: var(--gap);
                    .score_row_item {
                        align-self: end;
                        border-bottom: 1px solid white;
                        .key {
                            color: var(--red);
                        }
                    }
                }

            }
        }
        """

jsContent = ""

parseItems = \items ->
    items
    |> List.walk "" \acc, item -> Str.concat
            acc
            """
                <div class="score_row">
                    <div class="score_row_item"><span class="key">Name:</span> <span class="value">$(item.name)</span></div>
                    <div class="score_row_item"><span class="key">Score:</span> <span class="value">$(Num.toStr item.score)</span></div>
                </div>
            """
