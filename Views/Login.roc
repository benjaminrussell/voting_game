module [view]

view =
    html
        """
            $(htmlContent)

            <script>
                $(jsContent)
            </script>

            <style>
                $(cssContent)
            </style>     
        """

html = \string -> string
css = \string -> string
# js = \string -> string

htmlContent =
    html
        """
        <section id="login_view" class="page_view">
            <form id="login_form" class="card" action="/api/init" method="PUT">
                    <div id="login_text">
                        <span id="title">Login</span>
                        <span id="blerb">108% secure</span>
                        <div class="devider"></div>
                    </div>
                    <div id="login_inputs">
                        <label required id="name_input" class="label">
                            Name
                            <input type="text" name="name" class="input" />
                        </label>
                        <button  class="input">
                            submit
                        </button>
                    </div>
            </form>
        <section>
        """

cssContent =
    css
        """
        @scope(#login_view){
            #login_form {
                place-self: center;
                grid-template-rows: auto 1fr auto;
                grid-area: content;
                #login_text {
                    display: grid;
                    grid-template-rows: auto auto;
                    text-align: center;
                    #title {
                        font-size: 2.5rem;
                        font-weight: 800;
                        color: var(--frost_1);
                    }
                    
                }
                #login_inputs {
                    display: grid;
                    grid-template-rows: auto auto;
                    gap: var(--gap);
                    align-self: center;
                    #name_input {
                        display: grid;
                        grid-template-rows: auto 1fr;
                        align-self: center;
                    }
                    
                }
            }       
        }
        """

jsContent = ""
