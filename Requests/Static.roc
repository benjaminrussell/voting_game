module [handleStatic]
import "./Assets/favi.png" as favi : List U8
import "./Assets/main.css" as mainCss : List U8
import "./Assets/styles/vars.css" as varsCss : List U8
import "./Assets/styles/inputs.css" as inputsCss : List U8
import "./Assets/fonts/plex.woff2" as plex : List U8

handleStatic : List Str -> List U8
handleStatic = \urlParts ->
    when urlParts is
        ["robots.txt"] -> robotsTxt
        ["favicon.ico"] -> favi
        ["main.css"] -> mainCss
        ["styles", "vars.css"] -> varsCss
        ["styles", "inputs.css"] -> inputsCss
        ["fonts", "plex.woff2"] -> plex
        _ -> robotsTxt

robotsTxt : List U8
robotsTxt =
    """
    User-agent: *
    Disallow: /
    """
    |> Str.toUtf8
