module [handleReq]
import pf.Task exposing [Task]
import pf.Http exposing [Request, Response]
import pf.Url
import Requests.Static
import Requests.Router
import Requests.Api

makeResponse : List U8 -> Task Response []_
makeResponse = \bytes ->
    Task.ok {
        status: 200,
        headers: [
        ],
        body: bytes,
    }

handleReq : Request -> Task Response _
handleReq = \req ->
    urlSegments =
        req.url
        |> Url.fromStr
        |> Url.path
        |> Str.split "/"
        |> List.dropFirst 1

    when (req.method, urlSegments) is
        (Get, [""]) -> Requests.Router.handleRoute req
        (Get, ["view", ..]) -> Requests.Router.handleRoute req
        (_, ["api", ..]) -> Requests.Api.handleApi req
        (Get, ["robots.txt"]) -> makeResponse (Requests.Static.handleStatic urlSegments)
        (Get, ["favicon.ico"]) -> makeResponse (Requests.Static.handleStatic urlSegments)
        (Get, ["main.css"]) -> makeResponse (Requests.Static.handleStatic urlSegments)
        (Get, ["styles", ..]) -> makeResponse (Requests.Static.handleStatic urlSegments)
        (Get, ["fonts", ..]) -> makeResponse (Requests.Static.handleStatic urlSegments)
        _ -> Task.err (URLNotFound req.url)
