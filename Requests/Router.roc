module [handleRoute]
import pf.Task exposing [Task]
import pf.Http exposing [Request, Response]
import pf.Url
import Sql.Item
import Views.Home
import Views.Vote
import Views.Login
import Views.ScoreBoard
import AppView
import Helpers.Parsing exposing [parseCookie, parseUserFromCookies]
import Sql.User exposing [findUserByName]
import Sql.Item exposing [findItemById]

handleRoute : Request -> Task Response []_
handleRoute = \req ->

    urlParts =
        req.url
        |> Url.fromStr
        |> Url.path
        |> Str.split "/"
        |> List.dropFirst 1

    when urlParts is
        [""] -> makeResponse (Str.toUtf8 (AppView.wrapContent Views.Home.view))
        ["view", "home"] -> makeResponse (Str.toUtf8 (AppView.wrapContent Views.Home.view))
        ["view", "vote"] ->
            identifier = Result.withDefault (parseUserFromCookies (parseCookie req.headers)) { user: "fail" }
            user = findUserByName! identifier.user

            item = findItemById! (user.position + 1)
            makeResponse (Str.toUtf8 (AppView.wrapContent (Views.Vote.view user item)))

        ["view", "login"] -> makeResponse (Str.toUtf8 (AppView.wrapContent Views.Login.view))
        ["view", "scoreboard"] ->
            items = Sql.Item.allItems!
            makeResponse (Str.toUtf8 (AppView.wrapContent (Views.ScoreBoard.view items)))

        _ -> makeResponse (Str.toUtf8 (AppView.wrapContent Views.Home.view))

makeResponse : List U8 -> Task Response []_
makeResponse = \bytes ->
    Task.ok {
        status: 200,
        headers: [
        ],
        body: bytes,
    }
