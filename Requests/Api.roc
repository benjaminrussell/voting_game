module [handleApi]
import Sql.User exposing [updateUserPosition]
import Sql.Item exposing [updateItemScore]
import pf.Task exposing [Task]
import pf.Http exposing [Request, Response]
import pf.Url
import Helpers.Parsing exposing [parseLoginName, parseGivenScore]
import Helpers.Parsing exposing [parseCookie, parseUserFromCookies]
import Sql.User exposing [findUserByName]

handleApi : Request -> Task Response _
handleApi = \req ->
    urlParts =
        req.url
        |> Url.fromStr
        |> Url.path
        |> Str.split "/"
        |> List.dropFirst 1

    when (req.method, urlParts) is
        (Put, ["api", "init", ..]) ->
            newUser = parseLoginName req.body |> Task.fromResult!

            Sql.User.setUser newUser.name
            |> Task.attempt \result ->
                when result is
                    Ok {} -> initReponse newUser.name
                    Err err -> Task.err err

        (Put, ["api", "updateScore", ..]) ->
            tryScore = parseGivenScore req.body |> Task.fromResult!
            identifier = Result.withDefault (parseUserFromCookies (parseCookie req.headers)) { user: "fail" }
            user = findUserByName! identifier.user
            items = Sql.Item.allItems
            test = items!
            newPosition = user.position + 1

            if (Num.toI64 (List.len test)) >= newPosition then
                updateItemScore (user.position + 1) (Num.toI64 tryScore.score)
                |> Task.attempt \result ->
                    when result is
                        Ok {} ->
                            updateUserPosition 1 user.id
                            |> Task.attempt \ur ->
                                when ur is
                                    Ok {} ->
                                        if (Num.toI64 (List.len test)) > newPosition then
                                            toVoteView
                                        else
                                            toScoreView

                                    Err err -> Task.err err

                        Err err -> Task.err err
            else
                toScoreView

        _ -> crash "sadge"

initReponse : Str -> Task Response []_
initReponse = \name ->

    Task.ok {
        status: 303,
        headers: [
            { name: "Set-Cookie", value: Str.toUtf8 "user=$(name); Max-Age=2592000; Path=/;" },
            { name: "Location", value: Str.toUtf8 "/view/vote" },
        ],
        body: Str.toUtf8 "",
    }

toVoteView : Task Response []_
toVoteView =

    Task.ok {
        status: 303,
        headers: [
            { name: "Location", value: Str.toUtf8 "/view/vote" },
        ],
        body: Str.toUtf8 "",
    }

toScoreView : Task Response []_
toScoreView =

    Task.ok {
        status: 303,
        headers: [
            { name: "Location", value: Str.toUtf8 "/view/scoreboard" },
        ],
        body: Str.toUtf8 "",
    }
