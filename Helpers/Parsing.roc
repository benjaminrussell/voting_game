module [parseLoginName, parseCookie, parseUserFromCookies, parseGivenScore]
import pf.Http

parseLoginName : List U8 -> Result { name : Str } [UnableToParseBodyTask _]_
parseLoginName = \bytes ->

    dict = Http.parseFormUrlEncoded bytes |> Result.withDefault (Dict.empty {})

    name <-
        Dict.get dict "name"
        |> Result.mapErr \_ -> UnableToParseBodyTask bytes
        |> Result.try

    Ok { name }

parseGivenScore : List U8 -> Result { score : U64 } [UnableToParseBodyTask _]_
parseGivenScore = \bytes ->

    dict = Http.parseFormUrlEncoded bytes |> Result.withDefault (Dict.empty {})

    scoreStr <-
        Dict.get dict "score"
        |> Result.mapErr \_ -> UnableToParseScoreFromBodyTask bytes
        |> Result.try

    score = Result.withDefault (Str.toU64 scoreStr) 0

    Ok { score }

parseCookie : List { name : Str, value : List U8 } -> { name : Str, value : List U8 }
parseCookie = \headers ->
    cookies = Result.withDefault (List.findFirst headers (\header -> header.name == "cookie")) { name: "fail", value: [] }

    cookies

parseUserFromCookies : { name : Str, value : List U8 } -> Result { user : Str } [UnableToParseCookies _]_
parseUserFromCookies = \cookies ->
    dict = Http.parseFormUrlEncoded cookies.value |> Result.withDefault (Dict.empty {})

    user <-
        Dict.get dict "user"
        |> Result.mapErr \_ -> UnableToParseCookies cookies.value
        |> Result.try

    Ok { user }
