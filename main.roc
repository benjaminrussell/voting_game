app [main] {
    pf: platform "https://github.com/roc-lang/basic-webserver/releases/download/0.5.0/Vq-iXfrRf-aHxhJpAh71uoVUlC-rsWvmjzTYOJKhu4M.tar.br",
}

import pf.Stderr
import pf.Task exposing [Task]
import pf.Http exposing [Request, Response]
import Requests.Gate

main : Request -> Task Response []
main = \req ->
    Requests.Gate.handleReq req |> Task.onErr handleErr

handleErr : _ -> Task Response []_
handleErr = \err ->

    (msg, code) =
        when err is
            _ -> (Str.joinWith ["SERVER ERROR"] " ", 500)

    {} <- Stderr.line msg |> Task.await

    Task.ok {
        status: code,
        headers: [],
        body: [],
    }

