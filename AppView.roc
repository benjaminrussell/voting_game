module [wrapContent]

html = \string -> string
css = \string -> string
js = \string -> string

wrapContent : Str -> Str
wrapContent = \content ->

    htmlContent =
        html
            """
            <html lang="en">
              <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <title>Cocktail Night!</title>
                <link rel="stylesheet" href="/main.css">
                <script src="https://unpkg.com/htmx.org@2.0.0"></script>
              </head>

                <body hx-boost="true" id="app_body">
                    <header></header>
                    <main>
                        <div id="route_view">
                            $(content)
                        </div>
                    </main>
                    <footer></footer>
                </body>
            </html>
            """

    cssContent =
        css
            """
            @scope (html) {
                body {
                    display: grid;
                    grid-template-areas: 
                    "header"
                    "main"
                    "footer";
                    grid-template-rows: 50px 1fr 50px;
                    margin: 0px;
                }
                header {
                    background: var(--dark_3);
                    grid-area: header;
                }
                main {
                    display: grid;
                    grid-area: main;
                    padding: 0 var(--gap);
                    #route_view {
                        display: grid;
                    }
                    
                }
                footer {
                    background: var(--dark_3);
                    grid-area: footer;
                }
            }
            """

    jsContent = ""

    view = { html: htmlContent, css: cssContent, js: jsContent }
    makePage view

makePage : { html : Str, js : Str, css : Str } -> Str
makePage = \pageParts ->
    html
        """
           $(pageParts.html)

           <script>
               $(pageParts.js)
           </script>

           <style>
               $(pageParts.css)
           </style>

        """

